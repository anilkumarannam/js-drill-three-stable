const filter = (elements, callBackFunction) => {
    const filteredArray = [];
    if (elements) {
        for (let index = 0; index < elements.length; index++) {
            let element = elements[index];
            if (callBackFunction(element,elements) === true) {
                filteredArray.push(element);
            }
        }
    }
    return filteredArray;
}

module.exports = filter;