const reduce = (elements, callBackFunction, startingValue) => {
    let initialValue = startingValue;
    if (elements && initialValue) {
        for (let index = 0; index < elements.length; index++) {
            initialValue = callBackFunction(initialValue, elements[index]);
        }
    } else if (elements) {
        initialValue = elements[0];
        for (let index = 1; index < elements.length; index++) {
            initialValue = callBackFunction(initialValue, elements[index]);
        }
    }
    return initialValue;
}

module.exports = reduce;