function each(elements, callBackFunction) {
    if (elements !== null && elements !== undefined) {
        for (let index = 0; index < elements.length; index++) {
            let element = elements[index];
            callBackFunction(element, index);
        }
    }
}

module.exports = each;