const each = require("./each");

const isFlatArray = array => {
    if (array) {
        let isArray = true;
        each(array, (element) => {
            isArray = isArray && !Array.isArray(element);
        });
        return isArray;
    }
}

const flatten = (elements, depth = 1) => {

    if (elements && depth > 0) {

        if (!isFlatArray(elements)) {
            let flatArray = [];
            let auxDept = depth;
            let arrayElements = elements;

            while (auxDept > 0 && !isFlatArray(arrayElements)) {
                flatArray = []
                each(arrayElements, (element) => {
                    flatArray = flatArray.concat(element);
                });
                auxDept--;
                arrayElements = flatArray;
            }
            return flatArray;
        }
        return elements;
    }
}

module.exports = flatten;