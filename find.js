const find = (elements, callBackFunction) => {
    if (elements) {
        for (let index = 0; index < elements.length; index++) {
            let element = elements[index];
            if(callBackFunction(element)){
                return element;
                break;
            }
        }
    }
    return undefined;
}

module.exports = find;