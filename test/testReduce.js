const printInConsole = require("../myLib/libFunctions");
const reduce = require("../reduce");
const elements = [6, 7];
const v = reduce(elements, (a, b) => a * b, 0);
printInConsole(v);