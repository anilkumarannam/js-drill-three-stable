const find = require("../find");

const items = [1, 2, 3, 4, 5, 5];

const foundValue = find(items, element => element > 4);

console.log(foundValue);