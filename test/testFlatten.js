const printInConsole = require("../myLib/libFunctions");
const flatten = require("../flatten");

const nestedArray = [1, 2, [3,[3]], [[[4]]]];

const flattenedArray = flatten(nestedArray,Infinity);

printInConsole(flattenedArray);