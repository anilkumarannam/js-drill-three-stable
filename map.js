function map(elements, callBackFunction) {
    if (elements) {
        const mappedArray=[];
        for (let index = 0; index < elements.length; index++) {
            let element = elements[index];
            mappedArray.push(callBackFunction(element,index,elements));
        }
        return mappedArray;
    }
    return;
}

module.exports = map;